﻿using AtlassianAssessment.Data;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace AtlassianAssessment.UtilityClasses
{
    public static class CommonFunctions
    {
        internal static string GetResourceValue(string ResourceName)
        {
            //Using URL variable from resources file
            string value = Resource.ResourceManager.GetString(ResourceName);
            return value;
        }
        internal static void GetUrl(IWebDriver Driver, string url)
        {
            try
            {
                string urlValue = GetResourceValue(url);
                Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(80);
                Driver.Navigate().GoToUrl(urlValue);
            }
            catch (Exception ex)
            {
                throw new Exception("Can not get the url");
            }
        }

        internal static Random _random = new Random();

        // Generates a random number within a range.      
        internal static int RandomNumber(int min, int max)
        {
            return _random.Next(min, max);
        }

        /*This function will be used to enter text to any textfield*/
        private static void EnterText(IWebElement element, string text)
        {
            try
            {
                element.Click();
                element.Clear();
                element.SendKeys(text);
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to enter the text in the field");
            }

        }
        //Method to clear chrome browser cache
        internal static void ClearCache(IWebDriver Driver)
        {
            try
            {
                Driver.Navigate().GoToUrl("chrome://settings/clearBrowserData");
                Thread.Sleep(5000);
                Driver.SwitchTo().ActiveElement();
                Thread.Sleep(5000);
                Driver.FindElement(By.CssSelector("* /deep/ #clearBrowsingDataConfirm")).Click();
                Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                throw new Exception("Can not clear the chrome browser cache");
            }

        }
        //Method to wait for an element explicitly until it is visible
        internal static void ExplicitWait(IWebDriver Driver, By element)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(element));
            }
            catch (Exception ex)
            {
                throw new Exception("Element is not visible");
            }
        }

        internal static void AssertStringEquals(string actual, string expected, string message)
        {
            try
            {
                Assert.AreEqual(expected, actual, message);
            }
            catch (Exception e)
            {
                throw new Exception("Actual result is not equal to expected result");
            }
        }
        internal static void EnterKeyPress(By objectName, IWebDriver driver)
        {

            try
            {
                IWebElement txtBox = driver.FindElement(objectName);
                txtBox.SendKeys(Keys.Enter);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

    }
}
