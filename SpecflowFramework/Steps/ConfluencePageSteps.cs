﻿using AtlassianAssessment.Drivers;
using AtlassianAssessment.Pages;
using AtlassianAssessment.UtilityClasses;
using OpenQA.Selenium;
using System.Data;
using TechTalk.SpecFlow;

namespace AtlassianAssessment.Steps
{
    [Binding]
    public sealed class ConfluencePageSteps
    {

        private string url;
        private string pageName;
        public static IWebDriver _driver { get; private set; }

        [Given(@"User naviagte to the Atlassian url")]
        public void GivenUserNaviagteToTheAtlassianUrl()
        {
            //Initialising the browser
            _driver = GetBrowser.InitBrowser(CommonFunctions.GetResourceValue("Browser"));
            //Navigate to the Atlassian url
            LoginPage loginPage = new LoginPage(_driver);
            url = loginPage.GetUrl();
            loginPage.GoToUrl(url);
        }
        [When(@"User enter username and password using (.*) credentials")]
        public void WhenUserEnterUsernameAndPasswordUsingAuthorCredentials(string user)
        {
            //Fill in login details
            HomePage homePage = new HomePage(_driver);
            homePage.EnterLoginDetails(user);
        }

        [When(@"Click on the confluence link")]
        public void WhenClickOnTheConfluenceLink()
        {
            //User navigates to confluence page
            HomePage homePage = new HomePage(_driver);
            homePage.NavigateToConfluence();
        }

        [Then(@"User lands on the confluence homepage")]
        public void ThenUserLandsOnTheConfluenceHomepage()
        {
            //Verify user lands on confluence page
            ConfluenceHomePage confluenceHomePage = new ConfluenceHomePage(_driver);
            confluenceHomePage.ConfluenceHomePageSuccess();
        }

        [When(@"User sign out from the Atlassian application")]
        public void WhenUserSignOutFromTheAtlassianApplication()
        {
            //User logs out of the confluence
            ConfluenceHomePage confluenceHomePage = new ConfluenceHomePage(_driver);
            confluenceHomePage.LogOut();
        }

        [Then(@"User logs out successfully")]
        public void ThenUserLogsOutSuccessfully()
        {
            //Verify user logs out successfully
            HomePage homePage = new HomePage(_driver);
            homePage.ConfluenceLogOutSuccess();
        }
        [When(@"Author clicks on create button")]
        public void WhenAuthorClicksOnCreateButton()
        {
            //Author creates a new page in confluence
            ConfluenceHomePage confleneceHomePage = new ConfluenceHomePage(_driver);
            confleneceHomePage.GoToCreatePage();
        }
        [When(@"Author sets the below (.*)")]
        public void WhenAuthorSetsTheBelow(int permNo)
        {
            //Sets up the permission
            ConfluencePage confluencePage = new ConfluencePage(_driver);
            confluencePage.SetPermissions(permNo);
        }

        [When(@"Author sets permissions for the users as below")]
        public void WhenAuthorSetsPermissionsForTheUsersAsBelow(Table table)
        {
            //Sets up the permissions as per user table in feature file
            ConfluencePage confluencePage = new ConfluencePage(_driver);
            confluencePage.SetUserPermission(table);
        }

        [Then(@"verify the following permissions for the user")]
        public void ThenVerifyTheFollowingPermissionsForTheUser(Table table)
        {
            //Table from feature file
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                ConfluencePage confluencePage = new ConfluencePage(_driver);
                if (row.ItemArray[0].ToString().Equals("yes"))
                {
                    if (row.ItemArray[1].ToString().Equals("yes"))
                    {
                        //If user has view and edit permissions on the page
                        confluencePage.VerifyPermissions_ViewEdit(pageName);
                    }
                    else if (row.ItemArray[1].ToString().Equals("no"))
                    {
                        //If user has only view permission on the page
                        confluencePage.VerifyPermissions_View(pageName);
                    }
                }
            }
        }

        [When(@"Enters the details in the page")]
        public void WhenEntersTheDetailsInThePage()
        {
            //Author fills in the page details
            ConfluencePage conflenecePage = new ConfluencePage(_driver);
            pageName = conflenecePage.FillPageDetails();
        }

        [When(@"Publish the page")]
        public void WhenPublishThePage()
        {
            //Author or user publish the page and save the changes on the page
            ConfluencePage conflenecePage = new ConfluencePage(_driver);
            conflenecePage.PublishPage();

        }
        [Then(@"Verify user can not find the page")]
        public void ThenVerifyUserCanNotFindThePage()
        {
            ConfluenceHomePage confleneceHomePage = new ConfluenceHomePage(_driver);
            confleneceHomePage.SearchPageAndVerifyDoesNotExists(pageName);
        }


    }
}