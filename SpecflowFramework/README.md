## Table of Contents
1. [General Info](#general-info)
2. [Technologies](#technologies)
3. [Installation](#installation)
4. [Test Data](#test-data)
5. [Run the script](#run-the-script)
6. [Reporting](#reporting)
7. [Future Implementation](#future-implementation)

## General Info
***
This is a miniature automation testing framework using Selenium C# with Specflow plus runner. Test automation scripts are created in BDD (Behaviour Driven Development) and input is given in scenario outlines features.
Features of the framework

1. SpecFlow is a testing framework which supports BDD (Behaviour Driven Development). It defines the flow of application in plain English language called Gherkin. 
The reason for choosing this is that it can easily be understand by all so can be extended and maintained easily.
2. Uses runner as Specflow+ Runner
3. Unit testing framework organises the tests for build and run.
4. Includes logging (Nlogs) and html reports (extent reports).
5. Browser factory supports multiple browsers like Chrome, Firefox, IE.
6. Page object model design pattern is used where web pages are represented as classes and elements on the page are defined as variables on the class to abstract the web UI.
7. Applied abstraction in the automation framework where locators such as xpath, css selectors in page class. These locators are utilized but are hidden from the tests.
8. All the methods and variables of a page are encapsulated in a single class in the framework,  so it is easy to maintain and extend the framework. 
![Alt text](/../main/Framework.png?raw=true "Structure of the framework")

## Technologies
***
1.Microsoft Visual Studio 2019 as IDE for Selenium C# codebase.
2.Download Specflow 2019 from extensions(Go to Extensions -> Manage Extensions -> Online -> Download Spec Flow for Visual Studio 2019 or appropriate version )
3.Add following dependencies as nuget packages:
        *Specflow runner
        *Selenium web driver
        *NUnit
        *Selenium Support

## Installation
***
1. Clone the solution from BitBucket repository and open the solution in local explorer.
2. Go to the folder and double click on .sln file.
3. Clean code is essential to the success of an automation framework and hence the automation framework is maintained in folders structure. 
To promote reusability across tests, the framework, and the tests, are structurally separate within the automation code base. 

## Test Data
***
1. Before executing the scripts/scenarios in the framework:
>Go to Data -> Resources.resx file and update the browser and other data parameter if you need to.
2. Remember to read the comments and instructions on each feature file and update the data in feature files if it needs to be. 
3. After updating the feature file, you should always build your solution again before running any scripts.

## Run the script
***
1. After opening the solution in your MS visual studio, go to Build -> Clean the solution and Build -> Build the solution
2. After the solution is successfully build, you will get the tests under your test explorer as shown in the screenshot
3. Once you get the tests, you can double click on any test and update the feature file as per your test and right click and run the test

## Reporting
***
1. Spec flow plus runner has inbuilt feature of reporting and logging
2. Once the script is run, the html report and logs are saved in your local machine inside the framework folder -> Test Results
3. These are sorted by date so latest report can be found at top
4. The report tells the percentage of journey passed or failed
5. If one test case is failed for the first time, spec flow also tries to run it 2 more times 
6. The failed test case reason is stated in red on the test reports
7. Logs can be used to find how the script is run and if there were any issues with the same

## Future Implementation
***
Few of the methods are incomplete with comments written to be implemented in future