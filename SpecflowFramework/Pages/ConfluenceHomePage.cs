﻿using AtlassianAssessment.Base;
using AtlassianAssessment.UtilityClasses;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace AtlassianAssessment.Pages
{
    class ConfluenceHomePage : BaseApplicationPage
    {
        public ConfluenceHomePage(IWebDriver driver) : base(driver)
        { }

        //Properties on the confluence homepage
        public By UserMenuButton => By.XPath("//div[1]/button[1]/span[1]/div[1]/span[1]/span[1]");
        public By LogOutLink => By.XPath("//span[contains(text(),'Log Out')]");
        public By LogOutButton => By.XPath("//span[contains(text(),'Log out')]");
        public By CreatePageButton => By.XPath("//button[@id='createGlobalItem']");

        /*This method is to verify if the user is landed on the confluence homepage or not*/
        internal void ConfluenceHomePageSuccess()
        {
            try
            {
                CommonFunctions.ExplicitWait(_driver, CreatePageButton);
                Assert.IsTrue(_driver.FindElement(CreatePageButton).Displayed, "User is not at the Confluence HomePage");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        /*The below method logs out the user from the confluence application*/
        internal void LogOut()
        {
            try
            {
                //Explicit wait is a function in common functions class that waits for an element to be present before throwing the exception
                CommonFunctions.ExplicitWait(_driver, UserMenuButton);
                _driver.FindElement(UserMenuButton).Click();
                CommonFunctions.ExplicitWait(_driver, LogOutLink);
                _driver.FindElement(LogOutLink).Click();
                CommonFunctions.ExplicitWait(_driver, LogOutButton);
                _driver.FindElement(LogOutButton).Click();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /*The below method creates a new page in confluence*/
        internal void GoToCreatePage()
        {
            try
            {
                CommonFunctions.ExplicitWait(_driver, CreatePageButton);
                _driver.FindElement(CreatePageButton).Click();
                ((IJavaScriptExecutor)_driver).ExecuteScript("return document.readyState").Equals("complete");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        internal void SearchPageAndVerifyDoesNotExists(string pageName)
        {
            //For future development
        }
    }
}
