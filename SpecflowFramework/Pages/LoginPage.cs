﻿using AtlassianAssessment.Base;
using AtlassianAssessment.UtilityClasses;
using NLog;
using OpenQA.Selenium;
using System;

namespace AtlassianAssessment.Pages
{
    class LoginPage : BaseApplicationPage
    {
        public LoginPage(IWebDriver driver) : base(driver)
        { }
        //variable
        private string url;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        /*This method is to get the base url from the resource file*/
        internal string GetUrl()
        {
            try
            {
                //Get url value from resource file
                url = CommonFunctions.GetResourceValue("AtlassianUrl");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return url;

        }

        /*This method is to navigate the user to the url specified in the resources file*/
        internal void GoToUrl(string url)
        {
            try
            {
                //Navigate to the url
                _driver.Navigate().GoToUrl(url);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            _logger.Info("Navigated to url");
        }
    }
}