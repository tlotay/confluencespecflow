﻿using AtlassianAssessment.Base;
using AtlassianAssessment.UtilityClasses;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Data;
using System.Threading;
using TechTalk.SpecFlow;

namespace AtlassianAssessment.Pages
{
    class ConfluencePage : BaseApplicationPage
    {
        public ConfluencePage(IWebDriver driver) : base(driver)
        { }
        //variable
        int randomNum;

        //Properties on the confluence create page
        public By ManagePermissionButton => By.XPath("//body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[3]/div[2]/div[1]/div[3]/div[1]/span[1]/div[1]/div[1]/span[1]/div[1]/button[1]");
        public By TitleTextArea => By.XPath("//body/div[@id='full-height-container']/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[3]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/textarea[1]");
        //Permissions objects
        public By SelectRestrictions1 => By.XPath("//div[contains(@id,'option-0')]/div");
        public By SetRestrictionOption => By.CssSelector(".css-lrg2au-singleValue span");
        public By PublishButton => By.XPath("//button[@id='publish-button']");
        public By SelectRestrictions3 => By.XPath("//div[contains(@id,'option-2')]/div");
        public By SelectRestrictions2 => By.XPath("//div[contains(@id,'option-1')]/div");
        public By ApplyPermissionButton => By.XPath("//span[contains(text(),'Apply')]");

        public By EditPageButton => By.XPath("//button[@id='editPageLink']");
        public By SearchBoxInput => By.XPath("//header/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]");

        public By PermissionsSearchBox => By.XPath("//span[contains(text(),'Type a user name or group')]");
        public By AddButton => By.XPath("//body/div[2]/div[3]/div[1]/div[3]/div[2]/section[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[3]/button[1]");

        /*This menthod is to fill in the page details after creating a new page on confluence
         * Currently this page only fills in the page title which is appended with a random number so that each page name
         * is unique.
         * There is a possiblity of extending this method by filling in all the details of the page.*/
        internal string FillPageDetails()
        {
            string pageName = "";
            try
            {
                CommonFunctions.ExplicitWait(_driver, TitleTextArea);
                _driver.FindElement(TitleTextArea).Click();
                //returns random number
                randomNum = CommonFunctions.RandomNumber(1000, 9999);
                pageName = "TestPage_" + randomNum;
                _driver.FindElement(TitleTextArea).SendKeys(pageName);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return pageName;
        }

        /*This method sets the permissions on the page based on feature file input*/
        internal void SetPermissions(int permissionNum)
        {
            try
            {
                //clcik permission button
                CommonFunctions.ExplicitWait(_driver, ManagePermissionButton);
                _driver.FindElement(ManagePermissionButton).Click();
                _driver.SwitchTo().ActiveElement();
                //dropdown select based on restriction input from feature file
                CommonFunctions.ExplicitWait(_driver, SetRestrictionOption);
                _driver.FindElement(SetRestrictionOption).Click();
                ((IJavaScriptExecutor)_driver).ExecuteScript("arguments[0].scrollIntoView(true);", _driver.FindElement(SetRestrictionOption));
                if (permissionNum == 1)
                {
                    _driver.SwitchTo().ActiveElement();
                    CommonFunctions.ExplicitWait(_driver, SelectRestrictions1);
                    _driver.FindElement(SelectRestrictions1).Click();
                }
                else if (permissionNum == 2)
                {
                    _driver.SwitchTo().ActiveElement();
                    CommonFunctions.ExplicitWait(_driver, SelectRestrictions2);
                    _driver.FindElement(SelectRestrictions2).Click();
                }
                else if (permissionNum == 3)
                {
                    _driver.SwitchTo().ActiveElement();
                    CommonFunctions.ExplicitWait(_driver, SelectRestrictions3);
                    _driver.FindElement(SelectRestrictions3).Click();
                }
                //apply permissions
                CommonFunctions.ExplicitWait(_driver, ApplyPermissionButton);
                _driver.FindElement(ApplyPermissionButton).Click();
                Thread.Sleep(2000);
                CommonFunctions.ExplicitWait(_driver, PublishButton);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        /*This method is set the user permissions as per the table specified in the feature file*/
        internal void SetUserPermission(Table table)
        {
            try
            {
                string userId = "";
                //Table from feature file
                var dataTable = TableExtensions.ToDataTable(table);
                foreach (DataRow row in dataTable.Rows)
                {
                    //clcik permission button
                    CommonFunctions.ExplicitWait(_driver, ManagePermissionButton);
                    _driver.FindElement(ManagePermissionButton).Click();
                    //_driver.SwitchTo().ActiveElement();
                    if (row.ItemArray[0].ToString() == "author")
                    {
                        userId = CommonFunctions.GetResourceValue("AtlassianUserAuthor");

                    }

                    else if (row.ItemArray[0].ToString() == "user1")
                    {
                        userId = CommonFunctions.GetResourceValue("AtlassianUserId1");

                    }
                    else if (row.ItemArray[0].ToString() == "user2")
                    {

                        userId = CommonFunctions.GetResourceValue("AtlassianUserId2");

                    }
                    _driver.SwitchTo().ActiveElement();
                    //CommonFunctions.ExplicitWait(_driver, PermissionsSearchBox);
                    _driver.FindElement(PermissionsSearchBox).Click();
                    _driver.FindElement(PermissionsSearchBox).SendKeys(userId);
                    //press enter to select
                    _driver.FindElement(PermissionsSearchBox).SendKeys(Keys.Enter);

                    CommonFunctions.ExplicitWait(_driver, AddButton);
                    _driver.FindElement(AddButton).Click();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        /*This method is to verify the permissions for a user.
         * If a user has view and edit permissions or only view permissions.*/
        internal void VerifyPermissions_ViewEdit(string pageName)
        {
            try
            {
                CommonFunctions.ExplicitWait(_driver, SearchBoxInput);
                _driver.FindElement(SearchBoxInput).Click();
                _driver.FindElement(SearchBoxInput).SendKeys(pageName);
                _driver.FindElement(SearchBoxInput).SendKeys(Keys.Enter);
                //Verify if user can view the page created by author
                CommonFunctions.ExplicitWait(_driver, By.XPath("//strong[contains(text(),pageName)]"));
                //Verify if user can view the page
                Assert.IsTrue(_driver.FindElement(By.XPath("//strong[contains(text(),pageName)]")).Displayed, "User can not view the page created");
                _driver.FindElement(By.XPath("//strong[contains(text(),pageName)]")).Click();
                //Thread.Sleep(3000);
                CommonFunctions.ExplicitWait(_driver, EditPageButton);
                _driver.FindElement(EditPageButton).Click();
                ((IJavaScriptExecutor)_driver).ExecuteScript("return document.readyState").Equals("complete");
                CommonFunctions.ExplicitWait(_driver, PublishButton);
                //Thread.Sleep(3000);
                Actions actions = new Actions(_driver);
                actions.SendKeys("Test description");
                actions.Build().Perform();
                PublishPage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        /*This method is to verify the view permissions of a user*/
        internal void VerifyPermissions_View(string pageName)
        {
            try
            {
                Assert.IsTrue(_driver.FindElement(By.XPath("//span[contains(text(), pageName)]")).Displayed, "User can not view the page created");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        /*This method is to publish the page after creating or editing*/
        internal void PublishPage()
        {
            try
            {
                _driver.SwitchTo().DefaultContent();
                //publish page
                CommonFunctions.ExplicitWait(_driver, PublishButton);
                _driver.FindElement(PublishButton).Click();
                CommonFunctions.ExplicitWait(_driver, SearchBoxInput);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
