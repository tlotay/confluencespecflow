﻿using AtlassianAssessment.Base;
using AtlassianAssessment.UtilityClasses;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace AtlassianAssessment.Pages
{
    class HomePage : BaseApplicationPage
    {
        public HomePage(IWebDriver driver) : base(driver)
        {
        }
        //Properties on the page
        public By UserIdTextbox => By.XPath("//input[@id='username']");
        public By ContinueButton => By.XPath("//button[@id='login-submit']");
        public By PasswordTextbox => By.XPath("//input[@id='password']");
        public By LogInButton => By.XPath("//button[@id='login-submit']");
        public By AtlassianHomePageLabel => By.XPath("//header/nav[1]/div[2]/div[1]/span[1]");
        public By SwitchToMenuButton => By.XPath("//header/nav[1]/div[1]/button[1]/span[1]/span[1]/span[1]");
        public By ConfluenceButton => By.XPath("//span[contains(text(),'Confluence')]");
        public By SearchBoxInput => By.XPath("//header/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]");

        //variables
        private string userId;
        private string password;
        private string password_encrypt;

        //This method fills in user details based on the logged in user input from feature file

        internal void EnterLoginDetails(string user)
        {
            try
            {
                if (user == "author")
                {
                    userId = CommonFunctions.GetResourceValue("AtlassianUserAuthor");
                    password = CommonFunctions.GetResourceValue("AtlassianPasswordAuthor");
                    //encrypt decrypt password 
                    //To be implemented 
                    //password_ = EncryptDecrypt.Decrypt(password_encrypt, "508FE662-E842-4CAC-B264-EF4A55DE8777");
                }

                else if (user == "user1")
                {
                    userId = CommonFunctions.GetResourceValue("AtlassianUserId1");
                    password = CommonFunctions.GetResourceValue("AtlassianPassword1");
                }
                else if (user == "user2")
                {

                    userId = CommonFunctions.GetResourceValue("AtlassianUserId2");
                    password = CommonFunctions.GetResourceValue("AtlassianPassword2");
                }
                CommonFunctions.ExplicitWait(_driver, UserIdTextbox);
                _driver.FindElement(UserIdTextbox).SendKeys(userId);
                _driver.FindElement(ContinueButton).Click();
                CommonFunctions.ExplicitWait(_driver, PasswordTextbox);
                _driver.FindElement(PasswordTextbox).SendKeys(password);
                _driver.FindElement(LogInButton).Click();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        internal void NavigateToConfluence()
        {
            try
            {
                CommonFunctions.ExplicitWait(_driver, SwitchToMenuButton);
                _driver.FindElement(SwitchToMenuButton).Click();
                CommonFunctions.ExplicitWait(_driver, ConfluenceButton);
                _driver.FindElement(ConfluenceButton).Click();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }
        internal void ConfluenceLogOutSuccess()
        {
            try
            {
                CommonFunctions.ExplicitWait(_driver, ContinueButton);
                Assert.IsTrue(_driver.FindElement(ContinueButton).Displayed, "User is not logged out of the Confluence page");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}