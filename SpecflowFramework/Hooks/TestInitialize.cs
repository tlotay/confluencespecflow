﻿using AtlassianAssessment.Drivers;
using System.Diagnostics;
using TechTalk.SpecFlow;

namespace AtlassianAssessment.Hooks
{
    [Binding]
    public class TestInitialize
    {
        //Runs before each scenario
        [BeforeScenario]
        public static void StartUp()
        {
            Process[] chromeDriverProcesses = Process.GetProcessesByName("chromedriver");
            //To kill any driver running in the system
            //Only kills chromedriver
            foreach (var chromeDriverProcess in chromeDriverProcesses)
            {
                chromeDriverProcess.Kill();
            }
        }

        //Runs after each scenario
        [AfterScenario]
        public static void BrowserClose()
        {
            GetBrowser.Cleanup();
        }

    }
}
