﻿Feature: Confluence_PageDelete_VerifyPermissions
Feature to create a page on confluence as a user (author of the page)
Sets permissions for users
Delete the page author created 
Verify the page is deleted for all the users of the page

###########################################################################
##Permission Parameter
##1 - No Restrictions - Anyone on Confluence can view and edit
##2 - Editing Restrictions - Anyone on Confluence can view, some can edit
##3 - Restrictions Apply - Only specific people can view and edit
###########################################################################

@ConfluenceFeatures
##Scenario to create a new page in confluence workspace, add users and delete the page, verify users can not access the page 
Scenario Outline: VerifyPermissions_PageHasNoRestrictions
##login to confluence as author
	Given User naviagte to the Atlassian url
	When User enter username and password using <author> credentials
	And Click on the confluence link
	Then User lands on the confluence homepage
##author creates a new page
    When Author clicks on create button
	And Enters the details in the page
##sets permissions on the page
    And Author sets the below <permission>
	And Author sets permissions for the users as below
	| user  | read permissions | view permissions |
	| user1 | yes              | yes              |
	| user2 | yes              | no               |
	And Publish the page
##author deletes the page
    And Author deletes the page
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
##Login as a team member user1
	When User enter username and password using <user1> credentials
	Then User lands on the confluence homepage
##verify the user1 cannot find the page
    Then Verify user can not find the page
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
##Login as a team member user2
	When User enter username and password using <user2> credentials
	Then User lands on the confluence homepage
##verify the user2 can not find the page
    Then Verify user can not find the page
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully

Examples:
| author | permission | user1 | user2 |
| author | 2          | user1 | user2 |
################END OF FEATURE################################