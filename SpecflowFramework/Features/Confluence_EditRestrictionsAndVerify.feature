﻿Feature: Confluence_EditRestrictionsAndVerify
Feature to open an existing page on confluence as a user (author of the page) with "Restrictions Apply" restrictions
Upgrade and downgrade permissions of a user of the page
Verify the permissions are modified for the user of the team

@ConfluenceFeatures
##Scenario to open an existing page, upgrade permissions of a user from view to edit and verify it has been applied correctly
Scenario Outline: VerifyUpgradeUsersPermissions
##login to confluence as author
	Given User naviagte to the Atlassian url
	When User enter username and password using <author> credentials
	And Click on the confluence link
	Then User lands on the confluence homepage
##author open an existing page with "Restrictions Apply" restrictions
    When Author opens an existing <page>
##sets permissions for the below user
    And Author sets permissions for the users as below
	| user  | view permissions | edit permissions |
	| user1 | no               | no               |  
	And Publish the page
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
##Login as a team member user1
	When User enter username and password using <user1> credentials
	Then User lands on the confluence homepage
##verify the user1 can not view or edit the page
    Then verify the following permissions for the user
	| view | edit |
	| no   | no   |  
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
##login to confluence as author
	Given User naviagte to the Atlassian url
	When User enter username and password using <author> credentials
	And Click on the confluence link
	Then User lands on the confluence homepage
##author open an existing page with "Restrictions Apply" restrictions
    When Author opens an existing <page>
##sets permissions for the below user/updrade the permissions
    And Author sets permissions for the users as below
	| user  | view permissions | edit permissions |
	| user1 | yes              | yes              |  
	And Publish the page
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
##Login as a team member user1
	When User enter username and password using <user1> credentials
	Then User lands on the confluence homepage
##verify the user1 can view or edit the page
    Then verify the following permissions for the user
	| view | edit |
	| yes  | yes  |  
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
Examples:
| author | user1 | page          |
| author | user1 | TestPage_3330 | 

@ConfluenceFeatures
##Scenario to open an existing page, downgrade permissions of a user from edit to view and verify it has been applied correctly
Scenario Outline: VerifyDowngradeUsersPermissions
##login to confluence as author
	Given User naviagte to the Atlassian url
	When User enter username and password using <author> credentials
	And Click on the confluence link
	Then User lands on the confluence homepage
##author open an existing page with "Restrictions Apply" restrictions
    When Author opens an existing <page>
##sets permissions for the below user
    And Author sets permissions for the users as below
	| user  | view permissions | edit permissions |
	| user2 | yes              | yes              |  
	And Publish the page
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
##Login as a team member user1
	When User enter username and password using <user> credentials
	Then User lands on the confluence homepage
##verify the user1 can not view or edit the page
    Then verify the following permissions for the user
	| view | edit |
	| yes  | yes  |    
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
##login to confluence as author
	Given User naviagte to the Atlassian url
	When User enter username and password using <author> credentials
	And Click on the confluence link
	Then User lands on the confluence homepage
##author open an existing page with "Restrictions Apply" restrictions
    When Author opens an existing <page>
##sets permissions for the below user/updrade the permissions
    And Author sets permissions for the users as below
	| user  | view permissions | edit permissions |
	| user1 | no               | no               |  
	And Publish the page
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
##Login as a team member user
	When User enter username and password using <user> credentials
	Then User lands on the confluence homepage
##verify the user1 can view or edit the page
    Then verify the following permissions for the user
	| view | edit |
	| no   | no   |    
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
Examples:
| author | user  | page          |
| author | user2 | TestPage_3330 |  
################END OF FEATURE################################