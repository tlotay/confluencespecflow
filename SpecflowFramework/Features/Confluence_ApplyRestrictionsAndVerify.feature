﻿Feature: Confluence_ApplyRestrictionsAndVerify
Feature to create a page on confluence as a user (author of the page)
Sets permission on the page as No Restrictions, Editing Restricted, Restrictions Apply
Verify the view and edit permission for author and other users of the team

###########################################################################
##Permission Parameter
##1 - No Restrictions - Anyone on Confluence can view and edit
##2 - Editing Restrictions - Anyone on Confluence can view, some can edit
##3 - Restrictions Apply - Only specific people can view and edit
###########################################################################
@ConfluenceFeatures
##Scenario to create a new page in confluence workspace, sets permissions as "No Restrictions" and verify restrictions for users in the team
Scenario Outline: VerifyPermissions_PageHasNoRestrictions
##login to confluence as author
	Given User naviagte to the Atlassian url
	When User enter username and password using <author> credentials
	And Click on the confluence link
	Then User lands on the confluence homepage
##author creates a new page
    When Author clicks on create button
	And Enters the details in the page
##sets permissions on the page
    And Author sets the below <permission>
	And Publish the page
##verify the author of the page can view and edit the page
    Then verify the following permissions for the user
	| view | edit |
	| yes  | yes  |
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
##Login as a team member user1
	When User enter username and password using <user1> credentials
	Then User lands on the confluence homepage
##verify the user1 can view and edit the page
    Then verify the following permissions for the user
	| view | edit |
	| yes  | yes  |
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
##Login as a team member user2
	When User enter username and password using <user2> credentials
	Then User lands on the confluence homepage
##verify the user2 can view and edit the page
    Then verify the following permissions for the user
	| view | edit |
	| yes  | yes  |
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully

Examples:
| author | permission | user1 | user2 |
| author | 1          | user1 | user2 |

@ConfluenceFeatures
##Scenario to create a new page in confluence workspace, edit permissions as "Editing Restrictions" and verify restrictions for users in the team
Scenario Outline: VerifyPermissions_PageHasEditedRestrictions
##login to confluence as author
	Given User naviagte to the Atlassian url
	When User enter username and password using <author> credentials
	And Click on the confluence link
	Then User lands on the confluence homepage
##author creates a new page
    When Author clicks on create button
	And Enters the details in the page
##sets permissions on the page
    And Author sets the below <permission>
	And Author sets permissions for the users as below
	| user  | read permissions | view permissions |
	| user1 | yes              | yes              |
	| user2 | yes              | no               |
	And Publish the page
##verify the author of the page can view and edit the page
    Then verify the following permissions for the user
	| view | edit |
	| yes  | yes  |
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
##Login as a team member user1
    Given User naviagte to the Atlassian url
	When User enter username and password using <user1> credentials
	And Click on the confluence link
	Then User lands on the confluence homepage
##verify the user1 can view and edit the page
    Then verify the following permissions for the user
	| view | edit |
	| yes  | no   |  
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
##Login as a team member user2
    Given User naviagte to the Atlassian url
	When User enter username and password using <user2> credentials
	And Click on the confluence link
	Then User lands on the confluence homepage
##verify the user2 can view and edit the page
    Then verify the following permissions for the user
	| view | edit |
	| yes  | no   |  
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
Examples:
| author | permission | user1 | user2 |
| author | 2          | user1 | user2 |

@ConfluenceFeatures
##Scenario to create a new page in confluence workspace, edit permissions as "Restrictions Apply" and verify restrictions for users in the team
Scenario Outline: VerifyPermissions_PageHasRestrictionsApply
##login to confluence as author
	Given User naviagte to the Atlassian url
	When User enter username and password using <author> credentials
	And Click on the confluence link
	Then User lands on the confluence homepage
##author creates a new page
    When Author clicks on create button
	And Enters the details in the page
##sets permissions on the page
    And Author sets the below <permission>
	And Author sets permissions for the users as below
	| user  | view permissions | edit permissions |
	| user1 | yes              | yes              |
	| user2 | no               | no               |  
	And Publish the page
##verify the author of the page can view and edit the page
    Then verify the following permissions for the user
	| view | edit |
	| yes  | yes  |
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
##Login as a team member user1
    Given User naviagte to the Atlassian url
	When User enter username and password using <user1> credentials
	And Click on the confluence link
	Then User lands on the confluence homepage
##verify the user1 can view and edit the page
    Then verify the following permissions for the user
	| view | edit |
	| no   | yes  |  
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
##Login as a team member user2
    Given User naviagte to the Atlassian url
	When User enter username and password using <user2> credentials
	And Click on the confluence link
	Then User lands on the confluence homepage
##verify the user2 can view and edit the page
    Then verify the following permissions for the user
	| view | edit |
	| yes  | no   |  
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully

Examples:
| author | permission | user1 | user2 |
| author | 3          | user1 | user2 |
################END OF FEATURE################################