﻿Feature: Confluence_DefaultRestrictionsVerify
Feature to create a page on confluence as a user (author of the page)
Use default permissions on the page created
Verify that all the users of the team can view and edit the page including author of the page

@ConfluenceFeatures
##Scenario to create a new page in confluence workspace and verify restrictions for users in the team
Scenario Outline: VerifyPermissions_PageHasDefaultRestrictions
##login to confluence as author
	Given User naviagte to the Atlassian url
	When User enter username and password using <author> credentials
	And Click on the confluence link
	Then User lands on the confluence homepage
##author creates a new page
    When Author clicks on create button
	And Enters the details in the page
##publish the page
	And Publish the page
##verify the author of the page can view and edit the page
    Then verify the following permissions for the user
	| view | edit |
	| yes  | yes  |
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
##Login as a team member user1
	When User enter username and password using <user1> credentials
	Then User lands on the confluence homepage
##verify the user1 can view and edit the page
    Then verify the following permissions for the user
	| view | edit |
	| yes  | yes  |
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully
##Login as a team member user2
	When User enter username and password using <user2> credentials
	Then User lands on the confluence homepage
##verify the user2 can view and edit the page
    Then verify the following permissions for the user
	| view | edit |
	| yes  | yes  |
##log out from confluence
	When User sign out from the Atlassian application
	Then User logs out successfully

Examples:
| author | user1 | user2 |
| author | user1 | user2 | 
################END OF FEATURE################################