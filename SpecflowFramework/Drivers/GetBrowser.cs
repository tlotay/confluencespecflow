﻿using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;

namespace AtlassianAssessment.Drivers
{
    class GetBrowser
    {
        private static IWebDriver Driver;
        public static IWebDriver InitBrowser(string browser)
        {
            switch (browser.ToLower())
            {
                case "chrome":
                    ChromeOptions options = new ChromeOptions();
                    //Driver = new ChromeDriver();
                    var path = AppDomain.CurrentDomain.BaseDirectory;
                    Driver = new ChromeDriver(path);
                    Driver.Manage().Timeouts().PageLoad = TimeSpan.FromMinutes(3);
                    break;
                case "ie":
                    Driver = new InternetExplorerDriver();
                    break;
                case "firefox":
                    Driver = new FirefoxDriver();
                    Driver.Manage().Timeouts().PageLoad = TimeSpan.FromMinutes(3);
                    break;
            }
            Driver.Manage().Window.Maximize();
            return Driver;
        }
        public static void Cleanup()
        {
            if (TestContext.CurrentContext.Result.Outcome == ResultState.Failure)
            {
                if (Driver != null)

                {
                    DateTime time = DateTime.Now;
                    string dateToday = "_date_" + time.ToString("yyyy-MM-dd") + "_time_" + time.ToString("HH-mm-ss");
                    var screenshot = ((ITakesScreenshot)Driver).GetScreenshot();
                    screenshot.SaveAsFile(@"\\SpecFlowProject.Specs\Screenshot" + dateToday + ".png", ScreenshotImageFormat.Png);
                }
            }
            //Close the browser
            Driver.Dispose();

        }

    }
}
